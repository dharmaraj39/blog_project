from django.test import TestCase, Client

# Create your tests here.
from django.contrib.auth import get_user_model
from django.urls import reverse
from .models import Post

class Blogtests(TestCase):

    def setUp(self):
        self.user = get_user_model().objects.create_user(
            username = 'testuser',
            email = 'test@email.com',
            password = 'pass'
        )

        self.post = Post.objects.create(
            title = 'this is title',
            body = 'this is body',
            author = self.user,
        )

    def test_str_representation(self):
        post = Post(title = 'this is title')
        self.assertEqual(str(post), post.title)
        
    def test_post_content(self):
        self.assertEqual(f'{self.post.title}', 'this is title')
        self.assertEqual(f'{self.post.body}', 'this is body')
        self.assertEqual(f'{self.post.author}', 'testuser')

    def test_post_list_view(self):
        response = self.client.get(reverse('home'))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'this is body')
        self.assertTemplateUsed(response, 'home.html')

    def test_post_detail_view(self):
        response = self.client.get('/post/1/')
        no_response = self.client.get('/post/100000/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(no_response.status_code, 404)
        self.assertContains(response, 'this is title')
        self.assertTemplateUsed(response, 'postdetail.html')


